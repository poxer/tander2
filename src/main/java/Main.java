public class Main {

    public static void main(String[] args) {
        System.out.println("Welcome to Tander XML!");

        TanderXML tanderXML = new TanderXML();
        tanderXML.setDBParam("91.243.113.208", "3307", "admin", "mysqlpassword");
        tanderXML.setN(1000);
        if( tanderXML.connectDB() ) {
            tanderXML.fillDB();
            tanderXML.readDBField();
            tanderXML.createXML("1.xml");
            tanderXML.transformXML("1.xml", "2.xml", "style.xsl");

            System.out.println("Sum of all fields: " + tanderXML.getSum("2.xml"));
            tanderXML.closeDBconnection();
        }
    }
}