import org.w3c.dom.*;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

/**
 * Main class for XML
 */
public class TanderXML {
    private ArrayList<Integer> nList = new ArrayList<>();
    private String port, userName, password, ip;
    private int n;
    private Connection dbcon;

    /**
     * Sets N
     * @param n
     */
    public void setN(int n){
        this.n = n;
    }

    /**
     * Restunes
     * @return
     */
    public ArrayList<Integer> getArN(){
        return nList;
    }

    /**
     * Transforems XML file format
     * @param inFileName
     * @param outFileName new file
     * @param styleFileName
     */
    public void transformXML(String inFileName, String outFileName, String styleFileName) {
        TransformerFactory transFactory = TransformerFactory.newInstance();

        try {
            StreamSource stylesource = new StreamSource(new File(styleFileName));
            Transformer transforemer = transFactory.newTransformer(stylesource);

            StreamSource in = new StreamSource(inFileName);
            StreamResult out = new StreamResult(outFileName);
            transforemer.transform(in, out);
        }
        catch (TransformerException e) { e.printStackTrace(); }
    }

    /**
     * Reads DB and fill nList
     */
    public void readDBField() {
        nList.clear();
        if( dbcon != null ) {
            try (PreparedStatement selectStatement = dbcon.prepareStatement("Select FIELD FROM TEST")) {
                ResultSet rs = selectStatement.executeQuery();
                while (rs.next()) {
                    nList.add(rs.getInt("FIELD"));
                }
            } catch (SQLException e) { e.printStackTrace(); }
        }
    }

    /**
     * Creates new XML file with N fields
     * @param fileName
     */
    public void createXML(String fileName) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            Element root = doc.createElement("entries");

            for( Integer nl: nList ) {
                Element entry = doc.createElement("entry");
                Element field = doc.createElement("field");
                field.appendChild(doc.createTextNode(nl.toString()));
                entry.appendChild(field);
                root.appendChild(entry);
            }

            doc.appendChild(root);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(fileName)));

        } catch (TransformerException|ParserConfigurationException|FileNotFoundException e) { e.printStackTrace(); }
    }

    /**
     * Sets DB connection parameters
     * @param ip address of DB
     * @param port address of DB
     * @param userName
     * @param password
     */
    public void setDBParam(String ip, String port, String userName, String password) {
        this.ip = ip;
        this.port = port;
        this.userName = userName;
        this.password = password;
    }

    /**
     * Establish DB connection
     */
    public boolean connectDB() {
        dbcon = DB.getConnection(ip, port, userName, password);
        if( dbcon != null ) {
            System.out.println("DB connection - success");
            return true;
        }
        else {
            System.out.println("DB connection - fail");
            return false;
        }
    }

    /**
     * Closes DB connection
     */
    public void closeDBconnection() {
        try {
            if( dbcon != null ) dbcon.close();
        } catch (SQLException e) { e.printStackTrace(); }
    }

    /**
     * Truncates and fills DB
     */
    public void fillDB(){
        if( dbcon != null ) {
            try {
                PreparedStatement truncateStatement = dbcon.prepareStatement("TRUNCATE TABLE TEST");
                truncateStatement.executeUpdate(); // truncate table
                dbcon.setAutoCommit(false); //commit transaction manually

                PreparedStatement fillStatement = dbcon.prepareStatement("INSERT INTO TEST VALUES(?);");

                int batchSize = 40000;

                Instant begin = Instant.now();

                for( int i = 1; i <= n; i++ ) {
                    fillStatement.setInt(1, i);
                    fillStatement.addBatch();

                    if (i % batchSize == 0) {
                        fillStatement.executeBatch();
                    }
                }
                fillStatement.executeBatch();

                Instant end = Instant.now();
                Duration time = Duration.between(begin, end);

                dbcon.commit();

                System.out.println("Batch exec time = "+time.toMillis()+"ms for N="+n+", batchSize="+batchSize);

                dbcon.setAutoCommit(true);
            } catch (SQLException e) { e.printStackTrace(); }
        }
    }

    /**
     * Calculates sum of all attributes in XML file
     * @param fileName
     * @return Sum of all attributes
     */
    public long getSum(String fileName) {
        long sum = 0;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File(fileName));

            Element root = doc.getDocumentElement();
            NodeList children = root.getChildNodes();

            for( int i = 0; i < children.getLength(); i++) {
                sum += Integer.parseInt(children.item(i).getAttributes().item(0).getNodeValue());
            }
        } catch (IOException|SAXException|ParserConfigurationException e) { e.printStackTrace(); }

        return sum;
    }
}