import java.sql.*;

/**
 * Class for work with database connections
 */
public class DB {
    public static Connection getConnection( String ip, String port, String userName, String password ) {
        Connection connection = null;
        String url = "jdbc:mysql://" + ip +":"+ port + "/botwar?autoReconnect=true";
        String driverName = "com.mysql.jdbc.Driver";

        try {
            Class.forName(driverName);
        }
        catch (Exception e){ System.out.println("Check classpath. Cannot load db driver: " + driverName); }

        try {
            connection = DriverManager.getConnection(url, userName, password);
        }
        catch (SQLException e){
            System.out.println("Cannot connect to db: " + url);
        }
        return connection;
    }
}