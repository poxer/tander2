import org.junit.BeforeClass;
import org.junit.Test;
import java.util.ArrayList;
import static org.junit.Assert.assertEquals;

public class testXML {
    static TanderXML tanderXML = new TanderXML();

    @BeforeClass
    public static void testATR() {
        tanderXML.setDBParam("91.243.113.208", "3307", "admin", "mysqlpassword");
    }

    @Test
    public void test1() throws Exception {
        tanderXML.connectDB();
        tanderXML.setN(3);
        tanderXML.fillDB();
        tanderXML.readDBField();
        ArrayList<Integer> N = tanderXML.getArN();
        assertEquals("[1, 2, 3]", N.toString());
        tanderXML.closeDBconnection();
    }

    @Test
    public void test2() throws Exception {
        tanderXML.connectDB();
        tanderXML.setN(0);
        tanderXML.fillDB();
        tanderXML.readDBField();
        ArrayList<Integer> N = tanderXML.getArN();
        assertEquals("[]", N.toString());
        tanderXML.closeDBconnection();
    }

    @Test
    public void test3() throws Exception {
        tanderXML.connectDB();
        tanderXML.setN(3);
        tanderXML.fillDB();
        tanderXML.readDBField();
        tanderXML.createXML("1.xml");
        tanderXML.transformXML("1.xml", "2.xml", "style.xsl");
        assertEquals(3*4/2, tanderXML.getSum("2.xml"));
        tanderXML.closeDBconnection();
    }

    @Test
    public void test4() throws Exception {
        tanderXML.connectDB();
        tanderXML.setN(10);
        tanderXML.fillDB();
        tanderXML.readDBField();
        tanderXML.createXML("1.xml");
        tanderXML.transformXML("1.xml", "2.xml", "style.xsl");
        assertEquals(10*11/2, tanderXML.getSum("2.xml"));
        tanderXML.closeDBconnection();
    }
}
